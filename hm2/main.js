/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function HamburgerException(message) {
    this.message = message;
    this.name = "HamburgerException";
}
// HamburgerError.prototype = Object.create(Error.prototype);

class Hamburger{
    constructor(sizeValue, stuffingValue){
        this._topping = {};
        this.size = sizeValue;
        this.stuffing = stuffingValue;
    }
    /* Размеры, виды начинок и добавок */
    static SIZE_SMALL = {
        type: 'size',
        value: 'small',
        name: 'SIZE_SMALL',
        price: 50,
        cal: 20
    }
    static SIZE_LARGE = {
        type: 'size',
        value: 'large',
        name: 'SIZE_LARGE',
        price: 100,
        cal: 40
    }
    static STUFFING_CHEESE = {
        type: 'stuffing',
        value: 'cheese',
        name: 'STUFFING_CHEESE',
        price: 10,
        cal: 20
    }
    static STUFFING_SALAD =  {
        type: 'stuffing',
        value: 'salad',
        name: 'STUFFING_SALAD',
        price: 20,
        cal: 5
    }
    static STUFFING_POTATO = {
        type: 'stuffing',
        value: 'potato',
        name: 'STUFFING_POTATO',
        price: 15,
        cal: 10
    }
    static TOPPING_MAYO = {
        type: 'topping',
        value: 'mayo',
        name: 'TOPPING_MAYO',
        price: 20,
        cal: 5
    }
    static TOPPING_SPICE = {
        type: 'topping',
        value: 'spice',
        name: 'TOPPING_SPICE',
        price: 15,
        cal: 0
    }

    /**
     * Добавить добавку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     *
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    addTopping(toppingValue){
        if(!typeof toppingValue === 'object' || !toppingValue.type === 'topping') throw new HamburgerException('no topping given');
        if(this._topping[toppingValue.name]) throw new HamburgerException('duplicate topping ' + toppingValue.name);
        this._topping[toppingValue.name] = toppingValue;
    }
    /**
     * Убрать добавку, при условии, что она ранее была
     * добавлена.
     *
     * @param topping   Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    removeTopping(toppingValue){
        if(!this._topping[toppingValue.name]) throw new HamburgerException('topping ' + toppingValue.name + ' does not exist');
        delete this._topping[toppingValue.name];
    }

    /**
     * Получить список добавок.
     *
     * @return {Array} Массив добавленных добавок, содержит константы
     *                 Hamburger.TOPPING_*
     */
    get toppings(){
        return Object.values(this._topping);
    }

    /**
     * Узнать размер гамбургера
     */
    get size(){
        return this._size;
    }

    set size(sizeValue){
        if(this._size) throw new HamburgerException('size already exist');
        if(!sizeValue || sizeValue.type !== 'size') throw new HamburgerException('no size given');
        this._size = sizeValue;
    }

    /**
     * Узнать начинку гамбургера
     */
    get stuffing(){
        return this._stuffing;
    }

    set stuffing(stuffingValue){
        if(this._stuffing) throw new HamburgerException('stuffing already exist');
        if(!stuffingValue || stuffingValue.type !== 'stuffing') throw new HamburgerException('no stuffing given');
        this._stuffing = stuffingValue;
    }

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена в тугриках
     */

    calculatePrice(){
        var toppingList = this.toppings;
        var toppingPrice = toppingList.length == 1 ? toppingList[0].price :  toppingList.length > 1 ? toppingList.reduce((accumulator, currentValue) => accumulator.price + currentValue.price) : 0;
        return this._size.price + this._stuffing.price + toppingPrice;

    }

    /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */
    calculateCalories () {
        var toppingList = this.toppings;
        var toppingCal = toppingList.length == 1 ? toppingList[0].cal :  toppingList.length > 1 ? toppingList.reduce((accumulator, currentValue) => accumulator.cal + currentValue.cal) : 0;
        return this._size.cal + this._stuffing.cal + toppingCal;
    }
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
// function HamburgerException (...) {  }
// маленький гамбургер с начинкой из сыра
try {
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
//добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
// // // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // // А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
//     //добавляем специи
//     hamburger.addTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.toppings.length); // 1
//     //удаляем специи
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Price without sauce: %f", hamburger.calculatePrice());
// // // Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.size === Hamburger.SIZE_LARGE); // -> false
    console.log("Is hamburger small: %s", hamburger.size === Hamburger.SIZE_SMALL); // -> true

    // var hamburger2 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.STUFFING_CHEESE);
}catch (e) {
    console.log(e.name + ': '+e.message);
}




