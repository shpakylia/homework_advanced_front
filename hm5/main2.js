
//get main promise with movies data
function loadMovies(url) {
    return fetch(url)
        .then(response=>response.json())
        .then(
            (data)=>{
                if(data['results']){
                    return data['results'];
                }else {
                    throw new Error('empty response');
                }
            }
        )
        .catch((error)=>{console.error(error)});
}

function getResponse(url) {
    return fetch(url)
        .then(response=>response.json());

}

//display movies
function printMovies(movies) {
    document.querySelector('.loader').style = "display: none";
    let selector = document.querySelector('.movies-list');
    movies.forEach(val=>{
        let el = document.createElement('div');
        el.dataset.episodeId = val['episode_id'];
        el.className = 'movie-item';
        el.innerHTML = `<h3 class="movie-item-title">${val['title']}</h3>
<p><span class="movie-item-subtitle">Episode:</span>${val['episode_id']}</p>        
<p><span class="movie-item-subtitle">Description:</span>${val['opening_crawl']}</p>        
`;
        selector.append(el);
    });
    return movies;
}

//display characters
function printCharacters(data, mov){
    let characterBlock = createCharacterBlock(mov);
    characterBlock.innerHTML += data.map(val=> val['name']).join(', ');
}


function createCharacterBlock(mov){
    let characterBlock = document.createElement('div');
    characterBlock.className = 'characters';
    let selectorByEpisode = document.querySelector(`[data-episode-id="${mov['episode_id']}"]`);
    selectorByEpisode.append(characterBlock);
    characterBlock.innerHTML += "<h4 class='movie-item-characters'>Characters:</h4>";
    return characterBlock;
}

let loadedMovies = loadMovies('https://swapi.co/api/films/');
loadedMovies
    .then(printMovies)
    .then(movies=>{
        movies.forEach(mov=>{
            if(mov['characters']){
                Promise.all(mov.characters.map(getResponse))
                    .then(charactersArray=>{
                        printCharacters(charactersArray, mov);
                    });
            }
        })
    });
