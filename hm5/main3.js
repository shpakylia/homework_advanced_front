// send request
function httpFunc(url) {
    let promise = new Promise(function (resolve, reject){

        let xhr = new XMLHttpRequest();
        xhr.open('GET',url);
        xhr.onload = function() {
            if (this.status == 200) {
                resolve(this.response);
            } else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error("Network Error"));
        };

        xhr.send();

});
    let response = promise.then(response=>{return JSON.parse(response)});
    return response;
}

//get main promise with movies data
function loadMovies(url) {
    return httpFunc(url)
        .then(
            (data)=>{
                if(data['results']){
                    return data['results'];
                }else {
                    throw new Error('empty response');
                }
            }
        )
        .catch((error)=>{console.error(error)});
}

function displayMovies(data){
    document.querySelector('.loader').style = "display: none";
    let selector = document.querySelector('.movies-list');
    data.forEach(val=>{
        let el = document.createElement('div');
        el.dataset.episodeId = val['episode_id'];
        el.className = 'movie-item';
        let characters = val.characters.map(char=> char['name']).join(', ');
        el.innerHTML = `<h3 class="movie-item-title">${val['title']}</h3>
        <p><span class="movie-item-subtitle">Episode:</span>${val['episode_id']}</p>        
        <p><span class="movie-item-subtitle">Description:</span>${val['opening_crawl']}</p>     
        <h4 class='movie-item-characters'>Characters:</h4> 
        ${characters};  
        `;
        selector.append(el);
    });



}

let loadedMovies = loadMovies('https://swapi.co/api/films/');
loadedMovies.then(movies => {
    return Promise.all(movies.map(movie => {
        return Promise.all([
            Promise.all(movie.characters.map(httpFunc)),
            Promise.all(movie.species.map(httpFunc)),
        ]).then(([characters, species])=>({...movie, characters, species}));
    }));
}).then(moviesWithCharacters => {
    displayMovies(moviesWithCharacters);
});
