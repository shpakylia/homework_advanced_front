

let playStartBlock = document.getElementById('play');

let btn = document.querySelector('.start-play');
btn.addEventListener('click', startGame);

function startGame(event) {
    let level = document.querySelector('[name=level]:checked');
    playStartBlock.style.display = 'none';
    try{
        let game = new Game(level.value, '#plaing-field');
        game.start();
    }catch (e) {
        console.log(e.name + ": " + e.message, e.stack);
    }
}

function GameExaption(msg) {
    this.message = msg;
    this.name = "GameExaption";
}
class Game{

    constructor(levelValue, gameSelectorValue, {colls = 5, raws = 5}={}){
        this.levels = {
            '1':{
                name: 'Легкий уровень',
                time: 1500
            },
            '2':{
                name: 'Средний уровень',
                time: 1000
            },
            '3':{
                name: 'Сложный уровень',
                time: 500
            }
        };
        this.level = +levelValue;
        this.gameSelector = gameSelectorValue;
        this.colls = colls;
        this.raws = raws;

        this._colors = {
            'current': 'blue',
            'success': 'green',
            'fail': 'red'
        };
        this._player = 0;
        this._computer = 0;
    }
    get level(){
        return this._level;
    }
    get time(){
        return this._level['time'];
    }

    set level(levelValue){
        if(isNaN(levelValue) || levelValue > 3) throw new GameExaption('level is not correct');
        this._level =this.levels[levelValue];
    }

    get gameSelector(){
        return this._gameSelector;
    }
    set gameSelector(gameSelectorValue){
        this._gameSelector = document.querySelector(gameSelectorValue);
    }

    get colls(){
        return this._colls;
    }

    set colls(collsValue){
        if(isNaN(collsValue) || +collsValue < 1) throw new GameExaption('call must be more than 1');
        this._colls = +collsValue;
    }
    get raws(){
        return this._raws;
    }
    set raws(rawssValue){
        if(isNaN(rawssValue) || +rawssValue < 1) throw new GameExaption('raws must be more than 1');
        this._raws = +rawssValue;
    }

    get maxResult(){
        return (this._raws * this._colls)/2;
    }

    /**
     * start game and make first move
     */
    start() {
        this._printCells();
        this._clickHandler = this._checkClick.bind(this);
        this._gameSelector.addEventListener('mouseup', this._clickHandler);
        this._setNextAction();
    }

    /**
     * display table and save random td to array
     * @private
     */
    _printCells(){
        let table = document.createElement('table');
        let tr = document.createElement('tr');
        let td = document.createElement('td');
        td.className = 'plaing-cell';
        for (let i=0; i < this._raws; i++){
            tr.appendChild(td.cloneNode(true));
        }
        for (let k=0; k < this._colls; k++){
            table.appendChild(tr.cloneNode(true));
        }
        this._gameSelector.appendChild(table);
        this._cells = [...table.querySelectorAll('td')];
        this._cells.sort(() => Math.random() - 0.5)
    }

    _deleteCells(){
        this._gameSelector.innerHTML = '';
    }

    /**
     * set next cell
     * @private
     */
    _setCurrentCell(){
        this._currentCell = this._cells.pop();
    }

    _drawCurrentCellByStatus(colorStatus = 'current'){
        this._currentCell.style = `background-color: ${this._colors[colorStatus]}`;
    }

    _setNextAction(){
        this._clickedCell = false;
        this._setCurrentCell();
        this._drawCurrentCellByStatus();
        if(this._timeId) {
            clearTimeout(this._timeId);
        }
        this._timeId = setTimeout(this._actByComputer.bind(this), this.time);
    }

    /**
     * check user click on cell
     * @param event
     * @private
     */
    _checkClick(event){
        let clickedCell = event.target;
        if(clickedCell === this._currentCell) {
            this._actByPlayer();
        }else{
            this._actByComputer();
        }
    }

    _actByPlayer() {
        this._drawCurrentCellByStatus('success');
        this._player++;
        this._checkResult();
    }
    _actByComputer(){
        this._drawCurrentCellByStatus('fail');
        this._computer++;
        this._checkResult();
    }

    /**
     * check if game continue or print result
     * @private
     */
    _checkResult(){
        if(this._computer >= this.maxResult || this._player >= this.maxResult ){
            this._printWinner();
            clearTimeout(this._timeId);
        }else{
            this._setNextAction();
        }
    }

    /**
     * print result and show start bloc
     * @private
     */
    _printWinner(){
        this._gameSelector.removeEventListener('mouseup', this._clickHandler);
        this._deleteCells();
        playStartBlock.style.display = 'block';
        let result = playStartBlock.querySelector('.result');
        if(!result){
            result = document.createElement('p');
        }
        result.className = 'result';
        if(this._computer >= this.maxResult){
            result.textContent =  `Компьютер выграл со счетом ${this._computer} : ${this._player} `;
            result.classList.add('fail');
        }
        else{
            result.textContent = `Вы выиграли со счетом ${this._player} : ${this._computer} `;
        }
        playStartBlock.prepend(result);
    }

}
