/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function HamburgerException(message) {
    this.message = message;
    this.name = "HamburgerException";
}
// HamburgerError.prototype = Object.create(Error.prototype);

function Hamburger (size, stuffing){
    if(!size) throw new HamburgerException('no size given');
    if(!stuffing) throw new HamburgerException('no stuffing given');
    if(size.type !== 'size' || stuffing.type !== 'stuffing')
        throw new HamburgerException('not correct input');
    this.size = size;
    this.stuffing = stuffing;
    this.toppingArr = {};
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    type: 'size',
    value: 'small',
    name: 'SIZE_SMALL',
    price: 50,
    cal: 20
};
Hamburger.SIZE_LARGE = {
    type: 'size',
    value: 'large',
    name: 'SIZE_LARGE',
    price: 100,
    cal: 40
};
Hamburger.STUFFING_CHEESE = {
    type: 'stuffing',
    value: 'cheese',
    name: 'STUFFING_CHEESE',
    price: 10,
    cal: 20
};
Hamburger.STUFFING_SALAD = {
    type: 'stuffing',
    value: 'salad',
    name: 'STUFFING_SALAD',
    price: 20,
    cal: 5
};
Hamburger.STUFFING_POTATO = {
    type: 'stuffing',
    value: 'potato',
    name: 'STUFFING_POTATO',
    price: 15,
    cal: 10
};
Hamburger.TOPPING_MAYO = {
    type: 'topping',
    value: 'mayo',
    name: 'TOPPING_MAYO',
    price: 20,
    cal: 5
};
Hamburger.TOPPING_SPICE = {
    type: 'topping',
    value: 'spice',
    name: 'TOPPING_SPICE',
    price: 15,
    cal: 0
};
/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping){
    if(!typeof topping === 'object' || !topping.type === 'topping') throw new HamburgerException('no topping given');
    if(this.toppingArr[topping.name]) throw new HamburgerException('duplicate topping ' + topping.name);
    this.toppingArr[topping.name] = topping;
}

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if(!this.toppingArr[topping.name]) throw new HamburgerException('topping ' + topping.name + ' does not exist');
    delete this.toppingArr[topping.name];
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return Object.values(this.toppingArr);
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var toppingList = this.getToppings();
    console.log(toppingList);
    console.log(toppingList.reduce((accumulator, currentValue) => accumulator.price + currentValue.price));
    var toppingPrice = toppingList.length == 1 ? toppingList[0].price :  toppingList.length > 1 ? toppingList.reduce((accumulator, currentValue) => accumulator.price + currentValue.price) : 0;
    return this.size.price + this.stuffing.price + toppingPrice;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    // return this.size.cal + this.stuffing.cal;
    var toppingList = Object.values(this.toppingArr);
    var toppingCal = toppingList.length == 1 ? toppingList[0].cal :  toppingList.length > 1 ? toppingList.reduce((accumulator, currentValue) => accumulator.cal + currentValue.cal) : 0;
    return this.size.cal + this.stuffing.cal + toppingCal;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
// function HamburgerException (...) {  }
// маленький гамбургер с начинкой из сыра
try {
    var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    //добавляем специи
    // hamburger.addTopping(Hamburger.TOPPING_SPICE);
    //удаляем специи
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Price without sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    console.log("Is hamburger small: %s", hamburger.getSize() === Hamburger.SIZE_SMALL); // -> false
// // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1

    var hamburger2 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.STUFFING_CHEESE);
}catch (e) {
    console.log(e.name + ': '+e.message);
}




